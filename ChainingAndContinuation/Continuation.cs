﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

public class Continuation
{
    public static void Main()
    {
        Task<int[]> taskOne = Task.Run(() =>
        {
            return GenerateArray();
        });

        Console.WriteLine(string.Join(",", taskOne.Result));

        Task<int[]> taskTwo = taskOne.ContinueWith((x) =>
        {
            return MultipleArray(taskOne.Result);
        });

        Console.WriteLine(string.Join(",", taskTwo.Result));

        Task<int[]> taskThree = taskTwo.ContinueWith((x) =>
        {
            return SortedArray(taskTwo.Result);
        });

        Console.WriteLine(string.Join(",", taskThree.Result));

        Task<double> taskFour = taskThree.ContinueWith((x) =>
        {
            return Average(taskThree.Result);
        });

        Console.WriteLine(taskFour.Result);
    }

    public static int[] GenerateArray()
    {
        int Min = 0;
        int Max = 20;

        int[] arr = new int[5];

        Random randNum = new();
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = randNum.Next(Min, Max);
        }

        return arr;
    }

    public static int[] MultipleArray(int[] arr)
    {
        int Min = 0;
        int Max = 5;
        Random random = new();
        int multiplier = random.Next(Min, Max);

        for (int i = 0; i < arr.Length; ++i)
            arr[i] *= multiplier;

        return arr;
    }

    public static int[] SortedArray(int[] arr)
    {
        return arr.OrderBy(x => x).ToArray();
    }

    public static double Average(int[] arr)
    {
        return Queryable.Average(arr.AsQueryable());
    }
}